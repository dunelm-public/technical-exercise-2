# Dunelm Pairing Exercise

This repo contains a set of instructions for the Dunelm pairing exercise.

## Pre Requisites

- NodeJS 12 or above (https://nodejs.org/en/)
- Git (https://git-scm.com/downloads)

## Exercise Summary

Below you'll find an Scenario, along with a link to a set of three tasks. Your objective is to deliver the functionality outlined in the tasks. 
## Scenario

The country is opening back up after lockdown! People are keen to get out and start travelling again. Your company wants to you to create a new service which finds the locations with the best weather based on a list of preferred places to travel.

### Notes
- The only requirement is the api needs to be written in nodejs
- Feel free to use any framework or boilerplate.
- Feel free to google if you need additional information.
- If you're unsure about anything during the exercise then please ask for clarification.
- Write tests where you feel they're required.


When you've read through this please move on to [Task 1](./docs/task1.md).


