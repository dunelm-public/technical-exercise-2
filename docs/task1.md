[Back to Readme](../README.md)

### Task #1

## Overview

You will need to create an API which fulfills the following **requirements**:
- Receives a POST request with a body that contains the locations (example of such a request is shown below)
```
{
  "locations": ["London", "Canterbury", "Brighton"]
}
```
- Fetches the current temperature and weather condition from the [OpenWeatherAPI](https://openweathermap.org/current) for each location and returns them in the API response to the user. (Example of a suitable response payload is shown below).

```
 [
   {
 		"location": "London",
 		"weather": {
 			"temp": 14,
 			"weatherCondition": "Raining"
 		}
 	},
 	{
 		"location": "Canterbury",
 		"weather": {
 			"temp": 15,
 			"weatherCondition": "Cloudy"
 		}
 	},
 	{
 		"location": "Brighton",
 		"weather": {
 			"temp": 21,
 			"weatherCondition": "Clear"
 		}
 	}
 ]
```

**Some helpful tips for retrieving weather data!**
 - OpenWeatherAPI provides the means to query for weather information here:
[https://openweathermap.org/current#severalid](https://openweathermap.org/current#severalid)
- To retrieve weather conditions for multiple locations in one call you can perform a get with the following URL:
	- `http://api.openweathermap.org/data/2.5/group?id={locationID1,locationID2}&units=metric&appid={APIKey}`

Note that you will need to pass locationID as a query param. We will supply the required API key at the start of the exercise.

We have provided a JSON file that contains location names and their corresponding locationIDs for you to use [here](../resources/locations.json)


### Some things to think about

Some things we would like you to think about and discuss how you would implement are:
-  How the application handles logging
-  How application handles errors
-  How you would approach testing the application
-  How would you sanitise or validate data?

Once complete, move on to [Task 2](./task2.md)
